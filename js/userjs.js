//輪播圖
$(document).ready(function(){
    // $('.banner.owl-carousel').owlCarousel({
    //     loop:true,
    //     margin:0,
    //     nav:true,
    //     items:1,
    //     dots:false,
    //     animateOut: 'fadeOut',
    //     autoplay:true,
    //     autoplayHoverPause:false
    // })		
    $('.choice.owl-carousel').owlCarousel({
    		loop:true,
    		nav:true,
    		animateOut: 'fadeOut',
    		autoplay:true,
    		autoplayHoverPause:false,
        responsive:{
          0:{
            items:1,
            margin:0,

          },
          768:{
            items:3,
            margin:10
          },
          1199:{
            items:5,
            margin:10
          }
        }
		})
    $('.choice .owl-next').text('');
    $('.choice .owl-prev').text('');
    $('.banner .owl-prev').text('');
    $('.banner .owl-next').text('');

    setTimeout(function(){
      $('.gsc-search-button input').removeAttr('src');
    },3000)   
    /*會員登入模組*/
    $('.memberCart').append($('#ulMember'));
    $('#ulMember').removeClass('nav-list _sub effect');
    /*購物車*/
    $('#ulMember li:last-child').after($('#acart'));
    /*footer*/
    $('footer.container-fluid').before($('form[method="post"]#form1'));
    //gotop
    $('.btn-group ul li:last-child').click(function () {
      $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    $('.productlist-main .breadcrumb').parent().parent().parent().parent().addClass('product-list-detail')
    //手機版本會員搬遷

    if($(window).width() > 767){
      //把控制項加入陣列       
        var fadein_index=[
                ".page-aboutus",
                ".page-delivery",
                ".page-place",
                ".page-order",
                ".page-service",
                ".page-qa",
                ".page-privacy",
                ".productlist",
                ".cart-list",
                ".member"
                ];
                fadein_index.forEach(function(e){
                  $(e).css('opacity','0');
                })
        //滾動animation
        //當視窗高度到達物件時的動作
        function scrollanimation(a, b) {
            $(a).each(function() {
                // Check the location of each desired element //
                var objectBottom = $(this).offset().top ;
                var windowBottom = $(window).scrollTop() + $(window).innerHeight() ;
                if (objectBottom < windowBottom) {
                    $(this).addClass(b);
                }
            });
        };

        //遇到class+animation
        $(window).on("load", function() {
              fadein_index.forEach(function(e){
                setTimeout(scrollanimation(e,'animated fadeInUp'),100);
              });       
            // $(window).scroll(function() {
            //     //複製以下新增
            //     fadein_index.forEach(function(e){
            //             scrollanimation(e,'animated fadeInUp');
            //     });

            // });
        });
    }

    // $('.memberCart').append($('#ulMember'));
    if ($(window).width() < 768) {
      $('.nav.navbar-nav.pull-right').append($('#ulMember'));

      (function($){
              $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                event.preventDefault(); 
                event.stopPropagation(); 
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
              });
          })(jQuery);
    }
  })

